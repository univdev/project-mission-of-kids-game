﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberInputManager : MonoBehaviour
{
    // Start is called before the first frame update
    private static string number = "";

    void Update() {
        AnswerManager.SetNumber(number);
    }

    public static void Clear() {
        number = "";
    }

    public static int Get() {
        return int.Parse(number);
    }

    public static void Set(string num) {
        number = num;
    }
    public static void Set(int num) {
        number = num.ToString();
    }
    public static void Push(string num) {
        number += num;
    }
    public static void Push(int num) {
        number += num.ToString();
    }
    public static string GetString() {
        return number;
    }
}
