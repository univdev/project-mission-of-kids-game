﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowSceneManager : MonoBehaviour
{
    public GameObject messageBox;
    public static bool isConnectedArduino = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        messageBox.SetActive(!isConnectedArduino);
    }

    public void SetConnectedArduino(bool flag) {
        isConnectedArduino = flag;
    }
}
