﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSceneManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static bool isVisible = true;
    public GameObject modal;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        modal.SetActive(!isVisible);
    }
}
