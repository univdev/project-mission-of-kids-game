﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameCamera : MonoBehaviour
{
    public Transform target;
    private bool isFollowTarget = false;
    private Vector3 standardPosition;
    private Vector3 distance;
    private Rigidbody rb;
    private Animator anim;
    private GameObject sceneManager;
    // Start is called before the first frame update
    void Start()
    {
        sceneManager = GameObject.FindGameObjectWithTag("GameController");
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();

        standardPosition = new Vector3();
    }

    // Update is called once per frame
    void Update()
    {
        if (isFollowTarget) {
            anim.enabled = false;
            if (standardPosition == new Vector3()) {
                standardPosition = transform.position;
                distance = standardPosition - target.position;
            }
            FollowTarget();
        }
    }

    void FollowTarget() {
        Vector3 destination = target.position + distance;
        Vector3 position = Vector3.Lerp(transform.position, destination, Time.deltaTime * 6f);

        rb.MovePosition(position);
    }

    public void setFollowTarget(bool flag) {
        isFollowTarget = flag;
    }

    public bool getFollowTarget() {
        return isFollowTarget;
    }

    public void ToggleFollowTarget() {
        isFollowTarget = !isFollowTarget;
    }

    public void AnimationEnd() {
        sceneManager.GetComponent<IngameSceneManager>().SetMode(PlayerSituations.DICE);
        sceneManager.GetComponent<IngameSceneManager>().ToggleTimer(true);
        ToggleFollowTarget();
    }
}
