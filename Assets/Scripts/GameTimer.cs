﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
    private static GameTimer instance = null;
    private IngameSceneManager sceneManager;
    private int time = 600;
    private IEnumerator timer;
    private Text timeText;

    public static GameTimer Instance {
        get {
            if (instance == null) {
                Text timerUI = GameObject.FindGameObjectWithTag("Timer GUI").transform.Find("Number Cover").GetComponent<Text>();
                instance = new GameTimer();
                instance.timeText = timerUI;
            }
            return instance;
        }
    }

    void Awake() {
        instance = new GameTimer();
    }    
    public void ToggleTimer(bool flag) {
        StartCoroutine("ProgressTimer");
    }
    private IEnumerator ProgressTimer() {
        while (time >= 0) {
            yield return new WaitForSeconds(1);
            time -= 1;
            SetTimeText();
        }
    }
    public int GetTime() {
        return time;
    }
    public void SetTime(int t) {
        time = t;
    }
    void SetTimeText() {
        timeText.text = time.ToString();
    }
}
