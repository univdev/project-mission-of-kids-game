﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class SuccessSceneManager : MonoBehaviour
{
    // Start is called before the first frame update
    private static Text text = null;
    private int score = 0;
    void Awake() {
        text = GameObject.Find("Score Text").GetComponent<Text>();
        int famePerScore = 10000;
        int score = Inventory.money + (famePerScore * Status.fame);
        this.score = score;
        text.text = score.ToString();

        StartCoroutine(Submit());
    }

    IEnumerator Submit() {
        WWWForm form = new WWWForm();
        form.AddField("score", this.score);
        if (this.score >= 1000) {
            form.AddField("archivementNames", "초보는 아니네");
            form.AddField("archivementDescriptions", "총 점수가 1,000점을 돌파했습니다.");
        }
        if (this.score >= 4000) {
            form.AddField("archivementNames", "손이 풀렸다");
            form.AddField("archivementDescriptions", "총 점수가 4,000점을 돌파했습니다.");
        }
        if (this.score >= 8000) {
            form.AddField("archivementNames", "고수가 나타났다");
            form.AddField("archivementDescriptions", "총 점수가 8,000점을 돌파했습니다.");
        }
        if (Status.fame >= 10) {
            form.AddField("archivementNames", "메달 맛보기");
            form.AddField("archivementDescriptions", "메달을 10개 이상 모았습니다.");
        }
        if (Status.fame >= 20) {
            form.AddField("archivementNames", "메달 수집가");
            form.AddField("archivementDescriptions", "메달을 20개 이상 모았습니다.");
        }
        if (Status.fame >= 40) {
            form.AddField("archivementNames", "메달 리스트");
            form.AddField("archivementDescriptions", "메달을 40개 이상 모았습니다.");
        }

        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost:8080/ranks", form)) {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError) {
                Debug.Log(www.error);
            } else {
                Debug.Log("Form upload complete!");
            }
        }
    }
}
