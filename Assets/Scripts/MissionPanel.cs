﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionPanel : MonoBehaviour
{
    // Start is called before the first frame update
    private static GameObject panel;
    private static Text title;
    private static Text description;
    private static Text rewardCoin;
    private static Text rewardMedal;
    private static Text penaltyCoin;
    private static Text penaltyMedal;
    
    void Awake() {
        panel = GameObject.Find("Quest Panel");

        Transform questChild = GameObject.FindGameObjectWithTag("Quest Dialog").transform;
        Transform rewardCoinBox = questChild.transform.Find("Coin Reward");
        Transform rewardMedalBox = questChild.transform.Find("Medal Reward");
        Transform penaltyCoinBox = questChild.transform.Find("Coin Penalty");
        Transform penaltyMedalBox = questChild.transform.Find("Medal Penalty");

        title = questChild.Find("Title").GetComponent<Text>();
        description = questChild.Find("Description").GetComponent<Text>();
        rewardCoin = rewardCoinBox.Find("Number").GetComponent<Text>();
        rewardMedal = rewardMedalBox.Find("Number").GetComponent<Text>();
        penaltyCoin = penaltyCoinBox.Find("Number").GetComponent<Text>();
        penaltyMedal = penaltyMedalBox.Find("Number").GetComponent<Text>();

        Show(false);
    }
    public static void Show(bool flag) {
        panel.SetActive(flag);
    }
    public static void SetTitle(string text) {
        title.text = text;
    }
    public static void SetDescription(string text) {
        description.text = text;
    }
    public static void SetRewardCoin(int number) {
        rewardCoin.text = number.ToString();
    }
    public static void SetRewardMedal(int number) {
        rewardMedal.text = number.ToString();
    }
    public static void SetPenaltyCoin(int number) {
        penaltyCoin.text = number.ToString();
    }
    public static void SetPenaltyMedal(int number) {
        penaltyMedal.text = number.ToString();
    }
    public static string GetTitle() {
        return title.text;
    }
    public static string GetDescription() {
        return description.text;
    }
    public static void Init() {
        SetTitle("");
        SetDescription("");
        SetRewardCoin(0);
        SetRewardMedal(0);
        SetPenaltyCoin(0);
        SetPenaltyMedal(0);
    }
}
