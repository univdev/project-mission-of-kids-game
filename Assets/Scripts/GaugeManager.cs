﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GaugeManager : MonoBehaviour
{
    // Start is called before the first frame update
    private static GameObject gauge;

    private void Awake() {
        gauge = GameObject.Find("Gauge");
        SetActive(false);
    }

    public static void SetActive(bool flag) {
        gauge.SetActive(flag);
    }
}
