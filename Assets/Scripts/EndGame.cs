﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    public AudioClip clip;
    public AudioSource source;
    // Start is called before the first frame update
    public void Start() {
        source.clip = clip;
    }
    public void Clicked() {
        source.Play();
        Application.Quit(0);
    }
}
