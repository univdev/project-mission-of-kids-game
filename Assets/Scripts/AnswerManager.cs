﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerManager : MonoBehaviour
{
    private static GameObject selectedCardObject;
    private static GameObject selectedNumbersObject;
    private static Image selectedCard;
    private static Text selectedNumbers;
    private static Sprite[] colorCardMaterials;

    private void Awake() {
        selectedCardObject = GameObject.Find("Selected Card");
        selectedCard = selectedCardObject.GetComponent<Image>();

        selectedNumbersObject = GameObject.Find("Selected Numbers");
        selectedNumbers = selectedNumbersObject.GetComponent<Text>();

        colorCardMaterials = new Sprite[3];
        colorCardMaterials[0] = Resources.Load<Sprite>("Materials/파란색");
        colorCardMaterials[1] = Resources.Load<Sprite>("Materials/노란색");
        colorCardMaterials[2] = Resources.Load<Sprite>("Materials/보라색");
    }

    private void Update() {
        if (!NumberInputManager.GetString().Equals("")) {
            SetNumber(NumberInputManager.GetString());
            SetActiveSelectedNumbers(true);
        } else {
            SetActiveSelectedNumbers(false);
        }
        if (!ColorInputManager.GetColor().Equals("")) {
            SetColorCard(ColorInputManager.GetColor());
            SetActiveSelectedCard(true);
        } else {
            SetActiveSelectedCard(false);
        }
    }

    public static void SetColorCard(string c) {
        if (c.Equals("")) return;

        Sprite sprite = null;
        switch (c) {
            case "Blue":
                sprite = colorCardMaterials[0];
                break;
            case "Yellow":
                sprite = colorCardMaterials[1];
                break;
            case "Purple":
                sprite = colorCardMaterials[2];
                break;
        }

        selectedCard.sprite = sprite;
    }

    public static void SetNumber(string number) {
        selectedNumbers.text = number;
    }

    public static void SetActiveSelectedCard(bool flag) {
        selectedCardObject.SetActive(flag);
    }

    public static void SetActiveSelectedNumbers(bool flag) {
        selectedNumbersObject.SetActive(flag);
    }

    public static void Clear() {
        NumberInputManager.Clear();
        ColorInputManager.Clear();
    }
}
