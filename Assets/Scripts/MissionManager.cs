﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionManager : MonoBehaviour
{
    // Start is called before the first frame update
    private static Mission mission;
    private static IngameSceneManager scene = null;

    void Awake() {
        scene = GameObject.FindGameObjectWithTag("GameController").GetComponent<IngameSceneManager>();
    }

    public static void SetMission(Mission m) {
        mission = m;
    }
    public static string GetAnswer() {
        return mission.anwser;
    }
    public static Mission GetMission() {
        return mission;
    }
    public static string GetMissionType() {
        if (!mission) return null;
        return mission.type;
    }
    public static void GiveRewards() {
        Reward rewards = mission.rewards;
        SetResult(rewards);
    }
    public static void GivePenalty() {
        Reward penalty = mission.penalty;
        SetResult(penalty);
    }
    private static void SetResult(Reward r) {
        if (r == null) return;

        int money = r.money;
        int medal = r.fame;
        int place = r.place;
        int time = r.time;
        string type = r.placeType;

        Inventory.money += money;
        Status.fame += medal;
        if (scene != null) {
            scene.SetTimer(scene.GetTime() + time);
            if (place >= 0) {
                switch (type) {
                    case "Add":
                        scene.PlayerMove(scene.GetPlayerPosition() + place);
                        break;
                    case "Absolute":
                        scene.PlayerMove(place);
                        break;
                }   
            }
        }
    }
    public static void Show(bool flag) {
        MissionPanel.Show(flag);
    }
    public static void Set() {
        string title = mission.title;
        string description = mission.description;
        Reward rewards = mission.rewards;
        Reward penalty = mission.penalty;

        MissionPanel.Init();
        
        MissionPanel.SetTitle(title);
        MissionPanel.SetDescription(description);

        if (rewards) {
            MissionPanel.SetRewardCoin(rewards.money);
            MissionPanel.SetRewardMedal(rewards.fame);
        }
        if (penalty) {
            MissionPanel.SetPenaltyCoin(penalty.money);
            MissionPanel.SetPenaltyMedal(penalty.fame);
        }
    }
}
