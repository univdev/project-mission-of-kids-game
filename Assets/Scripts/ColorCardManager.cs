﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorCardManager : MonoBehaviour
{
    // Start is called before the first frame update
    private static ColorCard[] colors = {
        new ColorCard("Blue", "1?F11cC6?61Q1A"),
        new ColorCard("Yellow", "1?F11?F21wDAQ1A"),
        new ColorCard("Purple", "1?F1?6948Q1A")
    };

    public static string GetColor(string uid) {
        string result = null;
        for (int i = 0; i < colors.Length; i += 1) {
            ColorCard color = colors[i];
            if (color.GetUID().Equals(uid)) {
                result = color.GetColor();
                break;
            }
        }
        return result;
    }
}
