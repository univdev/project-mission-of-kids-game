﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IngameNoticeTextManager : MonoBehaviour
{
    // Start is called before the first frame update
    private static GameObject acceptTextObject;
    private static GameObject successTextObject;
    private static GameObject denyTextObject;
    private static GameObject failTextObject;
    private static Text acceptText;
    private static Text successText;
    private static Text denyText;
    private static Text failText;

    void Awake() {
        GameObject noticePanel = GameObject.FindGameObjectWithTag("Notice Panel");

        acceptTextObject = noticePanel.transform.Find("Accept Text").gameObject;
        successTextObject = noticePanel.transform.Find("Success Text").gameObject;
        denyTextObject = noticePanel.transform.Find("Deny Text").gameObject;
        failTextObject = noticePanel.transform.Find("Fail Text").gameObject;

        acceptText = acceptTextObject.GetComponent<Text>();
        successText = successTextObject.GetComponent<Text>();
        denyText = denyTextObject.GetComponent<Text>();
        failText = failTextObject.GetComponent<Text>();

        Clear();
    }

    public static void SetActiveAcceptText(bool flag) {
        acceptTextObject.SetActive(flag);
    }
    public static void SetActiveSuccessText(bool flag) {
        successTextObject.SetActive(flag);
    }
    public static void SetActiveDenyText(bool flag) {
        denyTextObject.SetActive(flag);
    }
    public static void SetActiveFailText(bool flag) {
        failTextObject.SetActive(flag);
    }
    public static void Clear() {
        SetActiveAcceptText(false);
        SetActiveSuccessText(false);
        SetActiveDenyText(false);
        SetActiveFailText(false);
    }
}
