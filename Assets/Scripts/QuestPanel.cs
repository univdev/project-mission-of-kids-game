﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestPanel : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioClip startEffect;
    void OnAnimationStart() {
        if (startEffect == null) return;
        IngameSoundEffect.Change(startEffect);
        IngameSoundEffect.Play();
    }
    void OnAnimationEnd() {
        
    }
}
