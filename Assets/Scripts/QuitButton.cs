﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitButton : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioClip clip;
    public AudioSource source;
    // Start is called before the first frame update
    public void Start() {
        source.clip = clip;
    }
    public void Clicked() {
        source.Play();
        Application.Quit(0);
    }
}
