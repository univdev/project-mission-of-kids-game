﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorCard
{
    private string color;
    private string uid;

    public ColorCard(string color, string uid) {
        this.color = color;
        this.uid = uid;
    }
    public void SetColor(string color) {
        this.color = color;
    }
    public void SetUID(string uid) {
        this.uid = uid;
    }
    public string GetColor() {
        return color;
    }
    public string GetUID() {
        return uid;
    }
}
