﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceNumber : MonoBehaviour
{
    public static int currentNumber = 0;
    private static IngameSceneManager sceneManager;

    void Awake() {
        sceneManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<IngameSceneManager>();
    }
    void OnEventTrigger() {
        
        int step = sceneManager.GetPlayerPosition() + currentNumber;
        Debug.Log(step);
        sceneManager.PlayerMove(step);
        gameObject.SetActive(false);
    }
}
