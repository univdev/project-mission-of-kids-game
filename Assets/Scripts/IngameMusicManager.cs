﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameMusicManager : MonoBehaviour
{
    // Start is called before the first frame update
    private AudioSource source;
    void Start()
    {
        source = GetComponent<AudioSource>();
        source.Stop();
        StartCoroutine("Play", 2f);
    }

    IEnumerator Play(float cooldown) {
        yield return new WaitForSeconds(cooldown);
        source.Play();
    }
}
